package com.tesis.back.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;
import com.tesis.back.models.UsuarioModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUsuario extends JpaRepository<UsuarioModel, Integer> {
    
}
