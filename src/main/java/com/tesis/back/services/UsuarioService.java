
package com.tesis.back.services;

import com.tesis.back.interfaces.IUsuario;
import com.tesis.back.models.UsuarioModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsuarioService {
    @Autowired
    IUsuario repositorio;

    public List<UsuarioModel> listar(){
        return repositorio.findAll();
    }
    public UsuarioModel save(UsuarioModel usuario){
        return repositorio.save(usuario);
    }
    
}
