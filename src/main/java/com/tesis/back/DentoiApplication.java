package com.tesis.back;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DentoiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DentoiApplication.class, args);
	}

}
