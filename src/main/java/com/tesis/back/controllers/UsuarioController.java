package com.tesis.back.controllers;

import com.tesis.back.services.UsuarioService;
import com.tesis.back.models.UsuarioModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequestMapping("/usuario")
public class UsuarioController {
    @Autowired
    UsuarioService usuarioService;

    @GetMapping()
    public List<UsuarioModel> listar() {
        return usuarioService.listar();
    }
    
    @PostMapping()
    public UsuarioModel save(@RequestBody UsuarioModel usuario){
        return this.usuarioService.save(usuario);
    }
    
}
