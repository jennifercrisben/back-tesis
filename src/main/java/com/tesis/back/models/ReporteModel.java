package com.tesis.back.models;

import java.sql.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Reporte")
public class ReporteModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "America/Lima")
    @Column(columnDefinition="DATETIME")
    private Date fechaIni;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "America/Lima")
    @Column(columnDefinition="DATETIME")
    private Date fechaFin;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechaIni() {
        return this.fechaIni;
    }

    public void setFechaIni(Date fechaIni) {
        this.fechaIni = fechaIni;
    }

    public Date getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    
}
