package com.tesis.back.models;

import javax.persistence.*;

@Entity
@Table(name="ElementoInventario")
public class ElementoInventarioModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    
    private Long id;
    private String nombre;
    private int stock;
    private int stockSeguridad;
    private String estado;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStock() {
        return this.stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getStockSeguridad() {
        return this.stockSeguridad;
    }

    public void setStockSeguridad(int stockSeguridad) {
        this.stockSeguridad = stockSeguridad;
    }

    public String getEstado() {
        return this.estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }



}
